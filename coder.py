from math import sqrt, ceil


# todo: add args and return types
# todo: consider resignation from class to module function
# todo*: consider change file name to cipher and put in file decoder
# todo*: add docstrs
class Coder:
    def code(self, text: str, key: int) -> str:
        """
        Code provided text with provided key using crazy cipher algorithm.
        :param text: text to code
        :param key: key for shift part of cipher
        :return: coded text
        """
        result = self._text_to_alphabet_number_list(text)
        result = self._list_shift_by_n(result, key)
        result = 'x'
        # print(F"Code {text} with key={key}")
        # return "XXIII VIII XXII O IV _ XV O XIII XI XVIII O XVII IV O O"

    def _text_to_alphabet_number_list(self, text):
        _list = []
        # todo: one convention element -> letter, element, el
        for element in text:
            if element.isalpha():
                _list.append((ord(element)) - ord('A') + 1)
            else:
                _list.append(element)
        return _list

    def _list_shift_by_n(self, _list, n):
        # todo: one convention el, element, elem
        return [(el + n) if isinstance(el, int) else el for el in _list]

    def conversion_numbers_to_roman(self, number):
        # todo: consider change string:list to string:str and rename to roman_number
        # todo: rename var to pointer
        # todo: rename _list to roman_decoder
        # todo: rename number to arabic_number
        # todo: consider unpacking symbol, value = _list[i]
        # todo: rename conversion_numbers_to_roman _arabic_to_roman_number
        var = 0
        string = []
        _list = [('X', 10), ('IX', 9), ('V', 5), ('IV', 4), ('I', 1)]
        while number > 0:
            if _list[var][1] <= number:
                number -= _list[var][1]
                string.append(_list[var][0])
            else:
                var += 1
        return "".join(string)

    def _conversion_numbers_list_to_roman(self, number_list):
        result = []
        for number in number_list:
            result += [self.conversion_numbers_to_roman(number)]
        return result


    def _square_length(self, square):
        return ceil(sqrt(len(square)))

    def _transcription(self, square):
        result = []
        length = self._square_length(square)
        x = length ** 2 - len(square)
        square += x*['O']
        for i in range(length):
            for j in range(length):
                result.append(square[j * length + i])
        return result
