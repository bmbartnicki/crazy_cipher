# Zajawka
Pewien zwariowany naukowiec wymyślił szyfr łączący szyfr przestawieniowy, szyfr przesuwający i kodowanie liczb za pomocą cyfr rzymskich.
Napisz program szyfrujący (a jak starczy czasu to i deszyfrujący) realizujący ten szyfr
# Przykład
```
 T   A  J   N   E   _   H  A  S   L   O
[20, 1, 10, 14, 5, '_', 8, 1, 19, 12, 15]

klucz przesuniecia = 3
[23, 4, 13, 17, 8, '_', 11, 4, 22, 15, 18]

do liczby rzymskich
['XXIII', 'IV', 'XIII', 'XVII', 'VIII', '_', 'XI', 'IV', 'XXII', 'XV', 'XVIII']

dlugosc=11, najbliższy kwadrat=16, bok_kwadratu=4
dopełnij do kwadratur przy pomocy "O"
['XXIII', 'IV', 'XIII', 'XVII', 'VIII', '_', 'XI', 'IV', 'XXII', 'XV', 'XVIII', 'O', 'O', 'O', 'O', 'O']

Zapisz wierszami i zczytaj kolumnmi
XXIII	IV	XIII	XVII	
VIII	_	XI	IV	
XXII	XV	XVIII	O	
O	O	O	O	
XXIII VIII XXII O IV _ XV O XIII XI XVIII O XVII IV O O 

```
# Ściąga python
```python
ord('A') # 65
ord('B')  # 66
from math import sqrt, ceil
sqrt(14) # 3.7416573867739413
ceil(3.74) # 4 - zaokrąglenie w górę
l = "A"
n = "1"
l.isalpha() # True
n.isalpha() # False
len("ABC") # 3
3*["X"] # ["X", "X", "X"]
_list = []
_list.append("X") # ["X"]
_list.append("Y") # ["X", "Y"]
isinstance(1, int) # True
" ".join(["A", "B", "C"]) # 'A B C'

```
# Plan działania
kolejka:
```
Piszący     Pilot
Rafał       Maciej
Maciej      Jan
Jan         Rafał
```
Piszący udostępnia ekran
Pisze kod tak aby spassować testy
tylko  pilot może podpowiadać
po 5min w przypadku problemów podpowiadają i dsykutują wszyscy
Piszący ustala kolejny mały krok i pisze test dla kolejnej osoby

Po kilku rundach lub na koniec (w zależności jaki bałagan będzie w kodzie) podejmujemy decyzję
o rundach refaktoringu.
Piszący udostępnia ekran i proponuje poprawkę, dystkutujemy, piszący robi poprawkę, testy po poprawce muszą przechodzić
ś# Algorytm konwersji z arabskich na rzymskie
Stwórz listę dostępnych symboli i ich wartości, listę rozszerz do symboli "poprzedników"
```
X   10
IX  9
V   5
IV  4
I   1
```
Sprawdzając od największego symbolu kolejno:
1. sprawdz czy symbol <= od liczby
1.1 jeżeli tak to odejmij jego wartość od liczby i dopisz symbol do wyniku
1.2 jezeli nie to przejdź do następnego symbolu
2. Powtarzaj 1. aż nie wyzerujesz liczby

przykład
```
liczba = 21
symbol X 10

10<=21
    liczba=21-10=11
    wynik=X
10<=11
    liczba=11-10=1
    wynik=XX
10<=1
    symbol IX 9
9<=1
    symbol V 5
4<=1
    symbol IV 4
1<=1
    liczba=1-1=0
    wynik=XXI
liczba==0
    zakończ    
 ```
# Plan na kolejne spotkanie
kolejne iteracje dojo
1. implementacja _transcription / posucie testu test_can_code_text
2. poprawa metody code przy użyciu dostępnych metod / kolejne testy uszczelniające
3. Przejście przez todo i refaktoring
4. Analiza jakich metod potrzeba do napisania decode, a które będą do reuse
5. lecimy z tymi metodami Coding Dojo
6. test + implementacja decode
7. Cornercase'y: test+poprawka
8. Wspólna analiza kodu i refaktoring
