# Przykład brzydkiego, ale dzialajacego kodu
# Cały kod jest praktycznie w jednej linice
# Podejrzewam, że jutro już sam go nie będę rozumiał :P
from roman import fromRoman
from math import ceil, sqrt
from functools import reduce


def decode(secret, key=3):
    return "".join(list([chr(l + 64) if isinstance(l, int) else l for l in
                         [l - key if isinstance(l, int) else l for l in
                          map(lambda w: ("" if w == 50 else ("_" if w == 1000 else w)), map(fromRoman, [x for x in list(
                              reduce(lambda x, y: x + y, map(list, zip(*[
                                  secret.replace("O", "L").replace("_", "M").split(" ")[
                                  i:i + ceil(sqrt(len(secret.split(" "))))] for i in
                                  range(0, len(secret.split(" ")), ceil(sqrt(len(secret.split(" ")))))])))) if
                                                                                                        x != "O"]))]]))


print(decode("XXIII VIII XXII O IV _ XV O XIII XI XVIII O XVII IV O O"))
