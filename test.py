import pytest
from coder import Coder


def test_can_code_text():
    expected = "XXIII VIII XXII O IV _ XV O XIII XI XVIII O XVII IV O O"
    assert Coder().code("TAJNE_HALO", key=3) == expected

def test_conversion_numbers_list_to_roman():
    assert Coder()._conversion_numbers_list_to_roman([1, 2, 20, 24, 30]) \
         ==  ["I", "II", "XX", "XXIV", "XXX"]

# todo: rename _text_to_alphabet_number_list _text_to_list_of_alphabet_number
def test_can_split_text_to_list_of_alphabet_number():
    assert Coder()._text_to_alphabet_number_list("TE_ST") == [20, 5, '_', 19, 20]


# todo: rename  _list_shift_by_n _shift_all_lists_elements
# todo: rename test_can_check_shift_by_n test_can_shift_all_lists_elements
def test_can_check_shift_by_n():
    assert Coder()._list_shift_by_n([20, 5, 19, 20], 3) == [23, 8, 22, 23]
    assert Coder()._list_shift_by_n([20, 5, 19, 20], 5) == [25, 10, 24, 25]
    assert Coder()._list_shift_by_n([20, 5, '_', 19, 20], 5) == [25, 10, '_', 24, 25]


# todo: rename _square_length _minimal_square_wall_length_for_infill_list_in_it
# todo: rename test_can_check_square_length test_can_get_minimal_square_wall_length_for_infill_list_in_it
def test_can_check_square_length():
    assert Coder()._square_length(5 * ['1']) == 3
    assert Coder()._square_length(16 * ['1']) == 4


# todo: rename _transcription _square_transciption
# todo: rename test_can_column_inside_square test_can_perform_square_transciption
def test_can_column_inside_square():
    x = [1, 2, 3,
         4, 5, 6,
         7, 8, 9]

    y = [1, 4, 7,
         2, 5, 8,
         3, 6, 9]

    assert Coder()._transcription(x) == y

def test_can_column_inside_inequal_square():
    x = [1, 2, 3,
         4, 5, 6,
         7, 8]

    y = [1, 4, 7,
         2, 5, 8,
         3, 6, 'O']

    assert Coder()._transcription(x) == y

